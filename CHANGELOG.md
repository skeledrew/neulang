# Release Notes

## latest
### Fixed
- `include` now works in script mode, not only REPL mode
- `include` resolves same dir module path to that dir instead of working dir
- Change reserved `return` keyword to `x_return` in `x_do`

### Improved
- Non-callables in `x_do` are returned as is instead of raising `ValueError`
- Set classes created with `x_class` in the namespace

## 0.3.1 (2019-08-07)
### Fixed
- AST category detection in REPL

### Added
- Access the patterns defined in a module scope via `n.get_patterns()` method or `--patterns` CLI option
- `Todo` example

### Improved
- `--to-py` option now works in the REPL
- `add_adapt_handler` usable as a combo node
- `x_import` now supports the `aliases` keyword and auto binds
- `help` functionality made easily discoverable in the interpreter, and backend inherited from `pydoc` module

## 0.3.0 (2019-07-24)
### Changed
- Names defined in `adapter` intents now expanded into the current scope

### Added
- Manipulate a `Neulang` instance `n`'s namespace by passing a list of names to remove and/or a dict of attribs to add via `n.update_namespace()`
- Essential keyword constructs as callables (provisional)
- Transpile Neu to Python source via `n.to_py()` method or `--to-py` CLI option
- Import (include) other `.neu` modules in the main module
- Pub/sub API
- Pass optional namespace into `Neulang`
- Integrate `PubSub`, publish `adapt` intent parts
- `PubSub` instance available in main namespace
- Natural language module `neulang.natural.basics` available

### Improved
- Non-handler nodes in the adapter body now inserted into the handler definition
- Named groups in adapter intent regex can now be specified by `<<name>>`

## 0.2.2 (2019-06-18)
- TBA
## 0.2.1 (2019-03-26)
- TBA
## 0.2.0 (2019-03-24)
- TBA
## 0.1.0
- TBA
## 0.0.1
- Initial release
