# Contributing
Merge requests are welcome. By submitting patches to this project you agree to allow them to be redistributed under the project's license, according to the normal forms and usages of the open-source community.

Fork, then clone the repo:

    git clone git@gitlab.com:your-username/neulang.git

Setup your machine (assumes Python and Pip are installed):

    pip install -r requirements_dev.txt

Ensure the tests pass:

    pytest

Create a branch for your work:

    git checkout -b my-feature-or-fix

Make your change, add tests and make them pass:

    pytest

Make your code PEP8 compliant:

    black neulang

Push the branch to your fork and [submit a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) to the `dev` branch.

We will try to at least comment on the request within a few days. We may suggest some changes, improvements or alternatives.
